import 'package:flutter/material.dart';
import '../model/categroy.dart';

//ChangeNotifier的混入是不用管理听众
class ChildCategory with ChangeNotifier {
  List<BxMallSubDto> childCategoryList = [];
  int childIndex = 0;
  String categoryId = '4'; //大类ID
  String subId = ''; //小类ID
  int page = 1; //列表页页数
  String noMoreText = ''; //显示没有数据得文字
  getChildCategory(List<BxMallSubDto> list, String id) {
    childIndex = 0;
    categoryId = id;
    subId = ''; //点击大类时，把子类ID清空
    page = 1; //点击大类时，将page赋值为1
    noMoreText = '';
    BxMallSubDto all = new BxMallSubDto();
    all.mallSubId = '';
    all.mallCategoryId = '00';
    all.mallSubName = '全部';
    all.comments = 'null';
    childCategoryList = [all];
    childCategoryList.addAll(list);
    notifyListeners();
  }

  //改变子类索引
  changeChildIndex(index, String id) {
    page = 1; //点击大类时，将page赋值为1
    noMoreText = '';
    //传递两个参数，使用新传递的参数给状态赋值
    childIndex = index;
    subId = id;
    notifyListeners();
  }

  //增加page 得方法

  addPage() {
    page++;
    // notifyListeners();
  }
  changeNoMore(String text){
    noMoreText=text;
    notifyListeners();
  }
}
