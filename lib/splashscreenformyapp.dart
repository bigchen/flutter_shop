import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';
import 'pages/index_page.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SplashScreenForMyapp extends StatefulWidget {
  _SplashScreenForMyappState createState() => _SplashScreenForMyappState();
}

class _SplashScreenForMyappState extends State<SplashScreenForMyapp> {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 2,
      navigateAfterSeconds: IndexPage(),
      title: Text('欢迎',
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: ScreenUtil().setSp(30))),
      image: Image.network(
          'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3114548853,3388357631&fm=26&gp=0.jpg'),
      gradientBackground: LinearGradient(
          colors: [Colors.pinkAccent, Colors.pink],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight),
      backgroundColor: Colors.white,
      styleTextUnderTheLoader: TextStyle(),
      photoSize: 100.0,
      onClick: () => print('123'),
      loaderColor: Colors.blue,
      loadingText: Text('请稍后'),
    );
  }
  
}
