import 'package:flutter/material.dart';
// EXCLUDE_FROM_GALLERY_DOCS_END
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_alert/easy_alert.dart';

class ChartPage extends StatelessWidget {
  final List<charts.Series> seriesList = _createSampleData();
  final bool animate = false;
  // ChartPage(this.seriesList, {this.animate});
  // factory ChartPage.withSampleData() {
  //   return new ChartPage(
  //     _createSampleData(),
  //     // Disable animations for image tests.
  //     animate: false,
  //   );
  // }
  static List<charts.Series<OrdinalSales, String>> _createSampleData() {
    final data = [
      new OrdinalSales('2014', 25),
      new OrdinalSales('2015', 25),
      new OrdinalSales('2016', 100),
      new OrdinalSales('2017', 75),
    ];

    return [
      new charts.Series<OrdinalSales, String>(
        id: 'zdc',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        data: data,
      )
    ];
  }

  @override
  Widget build(BuildContext context) {
    _onSelectionChanged(charts.SelectionModel model) {
      final selectedDatum = model.selectedDatum;
      final measures = <String, num>{};
      if (selectedDatum.isNotEmpty) {
        selectedDatum.forEach((charts.SeriesDatum datumPair) {
          measures[datumPair.series.displayName] = datumPair.datum.sales;
          Alert.toast(context, "${datumPair.datum.year}年:${datumPair.datum.sales}",
              position: ToastPosition.top, duration: ToastDuration.short);
        });
      }
    }

    return Scaffold(
        appBar: AppBar(
          title: Text('图表中心'),
        ),
        body: Container(
            width: ScreenUtil().setWidth(750),
            height: ScreenUtil().setHeight(440),
            child: Row(
              children: <Widget>[
                Container(
                  width: ScreenUtil().setWidth(375),
                  height: ScreenUtil().setHeight(440),
                  child: InkWell(
                    onTap: () {
                      print('123');
                    },
                    child: charts.BarChart(
                      seriesList,
                      animate: animate,
                      selectionModels: [
                        new charts.SelectionModelConfig(
                          type: charts.SelectionModelType.info,
                          changedListener: _onSelectionChanged,
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  width: ScreenUtil().setWidth(375),
                  height: ScreenUtil().setHeight(440),
                  child: charts.BarChart(
                    seriesList,
                    animate: animate,
                  ),
                ),
              ],
            )));
  }
}

/// Sample ordinal data type.
class OrdinalSales {
  final String year;
  final int sales;
  OrdinalSales(this.year, this.sales);
}
