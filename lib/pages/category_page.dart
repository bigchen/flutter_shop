import 'package:flutter/material.dart';
import '../subpages/categroy_sub/leftcategorynav.dart';
import '../subpages/categroy_sub/rightcategorynav.dart';
import '../subpages/categroy_sub/categorygoodslist.dart';

class CategoryPage extends StatefulWidget {
  final Widget child;

  CategoryPage({Key key, this.child}) : super(key: key);

  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('商品分类')),
      body: Container(
        child: Row(
          children: <Widget>[
            LeftCatgegortNav(),
            new Column(
              children: <Widget>[RightCategoryNav(),CategoryGoodsList()],
            ),
          ],
        ),
      ),
    );
  }
}
