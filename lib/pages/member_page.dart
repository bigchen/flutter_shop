import 'package:flutter/material.dart';
import 'package:provide/provide.dart';
import '../provide/counter.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class MemberPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Text('会员中心'),
      ),
      body: Center(
        child: Provide<Counter>(builder: (context, child, counter) {
          return Text('${counter.val}',
              style: TextStyle(
                  fontSize: ScreenUtil().setSp(56), color: Colors.red));
        }),
      ),
    );
  }
}
