import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provide/provide.dart';
import 'dart:convert';
import '../../service/service_method.dart';
import '../../model/categroy.dart';
import '../../provide/childcategory.dart';
import '../../provide/category_goods_list.dart';
import '../../model/categorygood.dart';


//左侧大类导航
class LeftCatgegortNav extends StatefulWidget {
  _LeftCatgegortNavState createState() => _LeftCatgegortNavState();
}

class _LeftCatgegortNavState extends State<LeftCatgegortNav> {
  List list = [];
  var listIndex = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getCategory();
    _getGoodList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(180),
      decoration: BoxDecoration(
          border: Border(right: BorderSide(width: 1, color: Colors.black12))),
      child: ListView.builder(
        itemCount: list.length,
        itemBuilder: (BuildContext context, int index) {
          return _leftinkWell(index);
        },
      ),
    );
  }

  Widget _leftinkWell(int index) {
    bool isChilk = false;
    isChilk = (index == listIndex) ? true : false;
    return InkWell(
      onTap: () {
        setState(() {
          listIndex = index;
        });
        var childList = list[index].bxMallSubDto;
        var categoryId = list[index].mallCategoryId;
        Provide.value<ChildCategory>(context).getChildCategory(childList,categoryId);
        _getGoodList(categoryId: categoryId);
      },
      child: Container(
        height: ScreenUtil().setHeight(100),
        padding: EdgeInsets.only(left: 10.0, top: 20.0),
        decoration: BoxDecoration(
            color: isChilk ? Color.fromRGBO(236, 238, 239, 1.0) : Colors.white,
            border:
                Border(bottom: BorderSide(width: 1, color: Colors.black12))),
        child: Text(
          list[index].mallCategoryName,
          style: TextStyle(fontSize: ScreenUtil().setSp(28)),
        ),
      ),
    );
  }

//获取后台数据
  void _getCategory() async {
    await request('getCategory').then((val) {
      var data = json.decode(val.toString());
      CategoryModel categrory = CategoryModel.fromJson(data);
      setState(() {
        list = categrory.data;
        Provide.value<ChildCategory>(context)
            .getChildCategory(list[listIndex].bxMallSubDto,list[listIndex].mallCategoryId);
      });
      // list.data.forEach((item)=>print(item.mallCategoryName));
    });
  }

  void _getGoodList({String categoryId}) async {
    var data = {'categoryId': categoryId==null?'4':categoryId, 'categorySubId': "", 'page': 1};
    await request('getMallGoods', formData: data).then((val) {
      var data = json.decode(val.toString());
      CategoryGoodListModel goodlist = CategoryGoodListModel.fromJson(data);
      Provide.value<CategoryGoodsListProvide>(context)
          .getGoodsList(goodlist.data);
    });
  }
}
