import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provide/provide.dart';
import 'dart:convert';
import '../../provide/childcategory.dart';
import '../../model/categroy.dart';
import '../../service/service_method.dart';
import '../../provide/category_goods_list.dart';
import '../../model/categorygood.dart';
class RightCategoryNav extends StatefulWidget {
  _RightCategoryNavState createState() => _RightCategoryNavState();
}

class _RightCategoryNavState extends State<RightCategoryNav> {
  // List list=['名酒','宝丰','北京二锅头','舍得','五粮液','茅台','散白'];
  var itemIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
        // child: Text('${childCategory.childCategoryList.length}'),
        child: Provide<ChildCategory>(
      builder: (context, child, childCategory) {
        return Container(
            height: ScreenUtil().setHeight(80),
            width: ScreenUtil().setWidth(570),
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border(
                    bottom: BorderSide(width: 1, color: Colors.black12))),
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: childCategory.childCategoryList.length,
              itemBuilder: (context, index) {
                return _rightInkWell(index,
                    childCategory.childCategoryList[index]);
              },
            ));
      },
    ));
  }

  Widget _rightInkWell(int index, BxMallSubDto item) {
    bool isCheck = false;
    isCheck = (index == Provide.value<ChildCategory>(context).childIndex)
        ? true
        : false;
    return InkWell(
      onTap: () {
        Provide.value<ChildCategory>(context).changeChildIndex(index,item.mallSubId);
        _getGoodList();
      },
      child: Container(
          padding: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 10.0),
          child: Text(
            item.mallSubName,
            style: TextStyle(
                fontSize: ScreenUtil().setSp(28),
                color: isCheck ? Colors.pink : Colors.black),
          )),
    );
  }
   void _getGoodList() async {
    var data = {'categoryId':Provide.value<ChildCategory>(context).categoryId, 'categorySubId': Provide.value<ChildCategory>(context).subId, 'page': 1};
    await request('getMallGoods', formData: data).then((val) {
      var data = json.decode(val.toString());
      CategoryGoodListModel goodlist = CategoryGoodListModel.fromJson(data); 
      if(goodlist.data==null){
         Provide.value<CategoryGoodsListProvide>(context).getGoodsList([]);
        }else{
          Provide.value<CategoryGoodsListProvide>(context).getGoodsList(goodlist.data);
        }
    });
  }
}
