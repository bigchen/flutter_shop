import 'package:flutter/material.dart';
import '../../service/service_method.dart';
import 'dart:convert';
import '../../model/categorygood.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

//商品列表，可以上拉加载
class CategoryGoodsList extends StatefulWidget {
  @override
  _CategoryGoodsListState createState() => _CategoryGoodsListState();
}

class _CategoryGoodsListState extends State<CategoryGoodsList> {
  List list = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getGoodList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: ScreenUtil().setWidth(570),
        height: ScreenUtil().setHeight(990),
        child: ListView(
          children: <Widget>[_categoryGoods()],
        ));
  }

  void _getGoodList() async {
    var data = {'categoryId': '4', 'categorySubId': "", 'page': 1};
    await request('getMallGoods', formData: data).then((val) {
      var data = json.decode(val.toString());
      print('分类商品列表：>>>>>>>>>>>>>${data}');
      CategoryGoodListModel goodlist = CategoryGoodListModel.fromJson(data);
      list = goodlist.data;
      print('分类商品列表：>>>>>>>>>>>>>${goodlist.data[0].goodsName}');
    });
  }

  Widget _wrapList() {
    List<Widget> listWiget = list.map((val) {
      return InkWell(
        onTap: () {
          print('1');
        },
        child: Container(
          width: ScreenUtil().setWidth(280),
          color: Colors.white,
          padding: EdgeInsets.all(2.0),
          margin: EdgeInsets.only(bottom: 3.0),
          child: Column(
            children: <Widget>[
              Container(
                height: ScreenUtil().setWidth(230),
                child: Image.network(val.image),
              ),
              Text(
                val.goodsName,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: Colors.pink, fontSize: ScreenUtil().setSp(26)),
              ),
              Row(
                children: <Widget>[
                  Text('￥${val.oriPrice}'),
                  Text(
                    '￥${val.presentPrice}',
                    style: TextStyle(
                        color: Colors.black26,
                        decoration: TextDecoration.lineThrough),
                  )
                ],
              )
            ],
          ),
        ),
      );
    }).toList();
    return Wrap(
      spacing: 2, //2列
      children: listWiget,
    );
  }

  Widget _categoryGoods() {
    return Container(
      child: Column(
        children: <Widget>[_wrapList()],
      ),
    );
  }
}
