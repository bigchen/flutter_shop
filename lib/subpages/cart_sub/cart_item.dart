import 'package:flutter/material.dart';
import 'package:flutter_shop/model/cartinfo.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_shop/provide/cart.dart';
import 'package:flutter_shop/subpages/cart_sub/cart_count.dart';
import 'package:provide/provide.dart';

class CartItem extends StatelessWidget {
  final CartInfoModel item;
  CartItem(this.item);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(5.0, 2.0, 5.0, 2.0),
      padding: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 10.0),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
          bottom: BorderSide(
            width: 1,
            color: Colors.black12
        ))
      ),
      child: Row(
        children: <Widget>[
          _cartCheckBtn(context,item),
          _cartImage(item),
          _cartGoodsName(item),
          _carPrice(context,item)
        ],
      ),
    );
  }
  //多选按钮
  Widget _cartCheckBtn(context,item){
    return Container(
      child: Checkbox(
        value: item.isCheck,
        activeColor: Colors.pink,
        onChanged: (bool val){
          
        },
      ),
    );
  }

  //商品图片
  Widget _cartImage(item){
    return Container(
      width: ScreenUtil().setHeight(150),
      padding: EdgeInsets.all(3.0),
      decoration: BoxDecoration(
        border: Border.all(width: 1.0,color: Colors.black12)
      ),
      child: Image.network(item.images),
    );
  }

  //商品名称

  Widget _cartGoodsName(item){
    return Container(
      width: ScreenUtil().setWidth(300),
      padding: EdgeInsets.all(10.0),
      alignment: Alignment.topLeft,
      child: Column(
        children: <Widget>[
          Text(item.goodsName),
           Container(
            alignment: Alignment.centerLeft,
            child:  CartCount(),
          ),
         
        ],
      ),
    );
  }
  //商品价格
  Widget _carPrice(context,item){
    return Container(
      width: ScreenUtil().setWidth(140),
      alignment: Alignment.centerRight,
      child: Column(
        children: <Widget>[
          Text('￥${item.price}'),
          Container(
            child: InkWell(
              onTap: (){
                 //主要代码---------------start----------
                  Provide.value<CartProvide>(context).deleteOneGoods(item.goodsId);
                  //主要代码--------------end-----------
              },
              child: Icon(
                Icons.delete_forever,
                color:Colors.black26
              ),
            ),
          )
        ],
      ),
    );
  }

}