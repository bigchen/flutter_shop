import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CartCount extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().setWidth(170),
      margin: EdgeInsets.only(top: 5.0),
      decoration: BoxDecoration(
        border: Border.all(width: 1.0,color: Colors.black12)
      ),
      child: Row(
        children: [
          _reduceBtn(),
          _countArea(),
          _addBtn()
        ],  
      ),
    );
  }

  //减号按钮
  Widget _reduceBtn(){
    return InkWell(
      onTap: (){

      },
      child:  Container(
        //宽度
        width: ScreenUtil().setWidth(45),
        //高度
        height: ScreenUtil().setHeight(45),
        alignment: Alignment.center,
        // 盒子样式
        decoration:  BoxDecoration(
          color: Colors.white,
          //设置Border属性给容器添加边框
          border:  Border(
            right: BorderSide(width: 1.0,color: Colors.black12)
          ),
        ),
        child: Text('-'),
      )
    );
  }

  //加号按钮
  Widget _addBtn(){
    return InkWell(
      onTap: (){

      },
      child:  Container(
        //宽度
        width: ScreenUtil().setWidth(45),
        //高度
        height: ScreenUtil().setHeight(45),
        alignment: Alignment.center,
        // 盒子样式
        decoration:  BoxDecoration(
          color: Colors.white,
          //设置Border属性给容器添加边框
          border:  Border(
            left: BorderSide(width: 1.0,color: Colors.black12)
          ),
        ),
        child: Text('+'),
      )
    );
  }

  //中间数量显示区域
  Widget _countArea(){
    return   Container(
      //宽度
      width: ScreenUtil().setWidth(75),
      //高度
      height: ScreenUtil().setHeight(45),
      alignment: Alignment.center,
      color: Colors.white,
      child: Text('1'),
    );
  }
}