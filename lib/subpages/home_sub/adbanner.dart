import 'package:flutter/material.dart';
class AdBanner extends StatelessWidget {
  final String adPicture;

  AdBanner({Key key, this.adPicture}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        print('点击了广告');
      },
      child: Image.network(adPicture),      
    );
  }
}