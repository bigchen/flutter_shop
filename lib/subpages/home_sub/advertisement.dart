import 'package:flutter/material.dart';

class Advvertisement extends StatelessWidget {
  final String pictureAddress1;
  final String pictureAddress2;
  final String pictureAddress3;

  Advvertisement(
      {Key key,
      this.pictureAddress1,
      this.pictureAddress2,
      this.pictureAddress3})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        _image(pictureAddress1),
        _image(pictureAddress2),
        _image(pictureAddress3),
      ],
    );
  }

  Widget _image(_puctureAddress) {
    return Expanded(
      child: InkWell(
          onTap: () {
            print('点击了广告');
          },
          child: Image.network(_puctureAddress)),
    );
  }
}
