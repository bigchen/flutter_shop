import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_shop/routers/application.dart';

class Recommend extends StatelessWidget {
  final List recommandList;

  Recommend({Key key, this.recommandList}) : super(key: key);

  //标题
  Widget _titlWiget() {
    return Container(
      alignment: Alignment.topLeft,
      padding: EdgeInsets.fromLTRB(10.0, 2.0, 0, 5.0),
      decoration: BoxDecoration(
          //盒子样式
          color: Colors.white,
          border:
              Border(bottom: BorderSide(width: 0.5, color: Colors.black12))),
      child: Text('商品推荐', style: TextStyle(color: Colors.pink)),
    );
  }

  //商品单独项方法

  Widget _item(context,index) {
    return Container(
      height: ScreenUtil().setHeight(330),
      width: ScreenUtil().setWidth(250),
      padding: EdgeInsets.all(8.0),
      decoration: BoxDecoration(
          color: Colors.white,
          border:
              Border(left: BorderSide(width: 0.5, color: Colors.black12))),
      child: InkWell(
        onTap: (){
           Application.router.navigateTo(context, "/detail?id=${recommandList[index]['goodsId']}");
        },
        child: Column(
          children: <Widget>[
            Image.network(recommandList[index]['image']),
            Text('￥${recommandList[index]['mallPrice']}',style: TextStyle(
              color: Colors.green
            )),
            Text(
              '￥${recommandList[index]['price']}',
              style: TextStyle(
                  decoration: TextDecoration.lineThrough, color: Colors.red),
            )
          ],
        ),
      ),
    );
  }

  //横向列表
  Widget _recommedList() {
    return Container(
      height: ScreenUtil().setHeight(360),
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: recommandList.length,
        itemBuilder: (BuildContext context, int index) {
          return _item(context,index);
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(420),
      margin: EdgeInsets.only(top: 10.0),
      child: Column(
        children: <Widget>[_titlWiget(), _recommedList()],
      ),
    );
  }
}
