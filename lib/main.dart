import 'package:flutter/material.dart';
import 'package:provide/provide.dart';
import 'provide/counter.dart';
import 'provide/childcategory.dart';
import 'package:easy_alert/easy_alert.dart';
import 'pages/index_page.dart';
import 'provide/category_goods_list.dart';
import 'provide/cart.dart';
import 'provide/details_info.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter_shop/routers/routers.dart';
import 'package:flutter_shop/routers/application.dart';
void main() {
  //provide 状态管理
  var providers = Providers();
  var counter = Counter();
  var childCategory = ChildCategory();
  var categoryGoodsListProvide = CategoryGoodsListProvide();
  var dtailsInfoProvide=DetailsInfoProvide();
  var cartProvide=CartProvide();
  providers
    ..provide(Provider<Counter>.value(counter))
    ..provide(Provider<ChildCategory>.value(childCategory))
    ..provide(Provider<CategoryGoodsListProvide>.value(categoryGoodsListProvide))
    ..provide(Provider<CartProvide>.value(cartProvide))
    ..provide(Provider<DetailsInfoProvide>.value(dtailsInfoProvide));

  runApp(AlertProvider(child: ProviderNode(child: MyApp(), providers: providers)));
}


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final router=Router();
    //注入路由
    Routes.configureRoutes(router);
    Application.router=router;
    return Container(
      child: MaterialApp(
          title: '百姓生活+',
          onGenerateRoute: Application.router.generator,//注入自定义路由
          debugShowCheckedModeBanner: false, //右上角不显示debug图标
          theme: ThemeData(primaryColor: Colors.pink),
          home: IndexPage()),
    );
  }
}
